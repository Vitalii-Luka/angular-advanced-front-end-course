const openForm = document.querySelector('[data-messenger]');
const btnOpenForm = document.querySelector('[data-btn-open-form]');
const btnCloseForm = document.querySelector('[data-btn-close-form]');

btnOpenForm.addEventListener('click', () => {
    openForm.style.display = 'block';
    btnOpenForm.style.display = 'none';
});

btnCloseForm.addEventListener('click', () => {
    openForm.style.display = 'none';
    btnOpenForm.style.display = 'block';
});

const msgerForm = document.querySelector('.msger-inputarea');
const msgerInput = document.querySelector('.msger-input');
const msgerChat = document.querySelector('.msger-chat');

const BOT_MSGS = [
    'Привіт, як ти?',
    'Вибачте, якщо мої відповіді не є доречними. :))',
    'Вибачте! Я не розумію, що ти хочеш сказати.',
];

const BOT_IMG = 'https://cdn-icons-png.flaticon.com/512/8708/8708769.png';
const PERSON_IMG = 'https://cdn-icons-png.flaticon.com/512/1177/1177568.png';
const BOT_NAME = 'Bot';
const PERSON_NAME = 'Guest';

function formatDate(date) {
    const h = `0${date.getHours()}`;
    const m = `0${date.getMinutes()}`;

    return `${h.slice(-2)}:${m.slice(-2)}`;
}

function appendMessage(name, img, side, text) {
    const msgHTML = `
    <div class="msg ${side}-msg">
      <div class="msg-img" style="background-image: url(${img})"></div>
      <div class="msg-bubble">
        <div class="msg-info">
          <div class="msg-info-name">${name}</div>
          <div class="msg-info-time">${formatDate(new Date())}</div>
        </div>
        <div class="msg-text">${text}</div>
      </div>
    </div>
  `;

    msgerChat.insertAdjacentHTML('beforeend', msgHTML);
    msgerChat.scrollTop += 500;
}

function random(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

function botResponse() {
    const r = random(0, BOT_MSGS.length - 1);
    const msgText = BOT_MSGS[r];
    const delay = msgText.split(' ').length * 100;

    setTimeout(() => {
        appendMessage(BOT_NAME, BOT_IMG, 'left', msgText);
    }, delay);
}

msgerForm.addEventListener('submit', (event) => {
    event.preventDefault();

    const msgText = msgerInput.value;
    if (!msgText) return;

    appendMessage(PERSON_NAME, PERSON_IMG, 'right', msgText);
    msgerInput.value = '';

    botResponse();
});
